module rotation
   !
   !
   implicit none
   !
   integer, parameter   :: DP  = SELECTED_REAL_KIND(15,99)
   !
   contains

      !=----------------------------------------------------------------------------=!
         subroutine rotate(crot, c, total_bands, lambda)
      !=----------------------------------------------------------------------------=!
            !
            implicit none
            !
            complex(DP), intent(inout)    :: crot(:, :), c(:, :)
            real(DP), intent(inout)       :: lambda(:, :)
            integer, intent(inout)        :: total_bands
            real(DP), allocatable         :: eigtemp(:)
            !
            integer                       :: ngw, i, j
            !
            allocate(eigtemp(total_bands))
            !
            call crot_gamma2(crot, c, size(c, 1), total_bands, 1, 1, lambda, total_bands, eigtemp)
            !
         end subroutine rotate
      !=----------------------------------------------------------------------------=!
      !=----------------------------------------------------------------------------=!
      !
      !
      !
      !
      !=----------------------------------------------------------------------------=!
         SUBROUTINE crot_gamma2 ( crot, c, ngw, n, noffr, noff, lambda, nx, eig )
      !=----------------------------------------------------------------------------=!
            !  this routine rotates the wave functions to the Kohn-Sham base
            !  it works with a block-like distributed matrix
            !  of the Lagrange multipliers ( lambda ).
            !
            ! ... declare modules
            ! 
            IMPLICIT NONE
      
            ! ... declare subroutine arguments
      
            INTEGER,     INTENT(IN)    :: ngw, n, nx, noffr, noff
            COMPLEX(DP), INTENT(INOUT) :: crot(:,:)
            COMPLEX(DP), INTENT(IN)    :: c(:,:)
            REAL(DP),    INTENT(IN)    :: lambda(:,:)
            REAL(DP),    INTENT(OUT)   :: eig(:)
      
            ! ... declare other variables
            !
            REAL(DP), ALLOCATABLE :: vv(:,:), ap(:)
            INTEGER               :: i, j, k
      
            IF( nx < 1 ) THEN
              RETURN
            END IF
      
            ALLOCATE( vv( nx, nx ) )
      
            ! NON distributed lambda
      
            ALLOCATE( ap( nx * ( nx + 1 ) / 2 ) )
      
            !DO i = 1, n
            !      write(*,*) (lambda(i, j), j=1, n)
            !END DO

            K = 0
            DO J = 1, n
               DO I = J, n
                  K = K + 1
                  ap( k ) = lambda( i, j )
               END DO
            END DO
      
            CALL dspev_drv( 'V', 'L', n, ap, eig, vv, nx )
      
            !write(*,*)
            !DO i = 1, n
            !      write(*,*) (vv(i, j), j=1, n)
            !END DO

            DEALLOCATE( ap )
      
            !do i=1, n
            !   do j=1, ngw
            !      write(9300+i, *) c(j, i)
            !   enddo
            !enddo
            !
            crot = 0.0d0
            !
            DO j = 1, n
               DO i = 1, n
                  CALL daxpy( 2*ngw, vv(j,i), c(1,j+noff-1), 1, crot(1,i+noffr-1), 1 )
               END DO
            END DO
            !
            !do i=1, n
            !   do j=1, ngw
            !      write(9300+i,  *) c(j, i)
            !   enddo
            !enddo
            ! 
            DEALLOCATE( vv )
            ! 
            RETURN
         END SUBROUTINE crot_gamma2
      !=----------------------------------------------------------------------------=!
      !
      !
      !
      !
      !=----------------------------------------------------------------------------=!
        SUBROUTINE dspev_drv( JOBZ, UPLO, N, AP, W, Z, LDZ )
      !=----------------------------------------------------------------------------=!
        IMPLICIT NONE
        CHARACTER ::       JOBZ, UPLO
        INTEGER   ::       IOPT, INFO, LDZ, N
        REAL(DP) ::  AP( * ), W( * ), Z( LDZ, * )
        REAL(DP), ALLOCATABLE :: WORK(:)

        IF( n < 1 ) RETURN

        ALLOCATE( work( 3*n ) )

#if defined __ESSL
          IOPT = 0
          IF((JOBZ .EQ. 'V') .OR. (JOBZ .EQ. 'v') ) iopt = iopt + 1
          IF((UPLO .EQ. 'U') .OR. (UPLO .EQ. 'u') ) iopt = iopt + 20
          CALL DSPEV(IOPT, ap, w, z, ldz, n, work, 3*n)
#else 
          CALL DSPEV(jobz, uplo, n, ap(1), w(1), z(1,1), ldz, work, INFO)
          IF( info .NE. 0 ) THEN
            write(*,*) 'ERROR: dspev_drv diagonalization failed' 
            stop
          END IF
#endif

        DEALLOCATE( work )
 
        RETURN
      END SUBROUTINE dspev_drv
      !=----------------------------------------------------------------------------=!
      !
      !
end module rotation
