program main
   !
   !=--------------------------------------------------------------------------------------=!
   !
   ! IsoPrint 
   !
   ! Copyright 2014 Charles W. Swartz VI
   !
   ! This program is free software: you can redistribute it and/or modify
   ! it under the terms of the GNU General Public License as published by
   ! the Free Software Foundation, either version 3 of the License, or
   ! (at your option) any later version.
   !
   ! This program is distributed in the hope that it will be useful,
   ! but WITHOUT ANY WARRANTY; without even the implied warranty of
   ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   ! GNU General Public License for more details.

   ! You should have received a copy of the GNU General Public License
   ! along with this program.  If not, see <http://www.gnu.org/licenses/>.
   !=--------------------------------------------------------------------------------------=!
   !
   use iotk_module
   !
   implicit none
   ! 
   integer, parameter            :: DP  = SELECTED_REAL_KIND(15,99)
   integer, parameter            :: NUM_CLI = 3
   INTEGER, parameter            :: Mb=1024*1024, complex_size=16, real_size=8
   real, parameter               :: BOHR_TO_ANGSTROM = 0.52917721092 
   character(len=20), parameter  :: configfile = 'isoprint-config.in'
   character(len=20), parameter  :: dataroot = 'data-file.xml'
   character(len=20), parameter  :: gkroot = 'gkvectors.dat'
   character(len=20), parameter  :: lambdaroot = 'lambda'
   !
   integer                       :: dunit =11, gunit=12, eunit=13, cunit=14, lunit=15
   integer                       :: kpt,                 &  !K-point number of previous QE-Calculation
                                    bnum,                &  !Number of Bands **to plot** (Command line)
                                    nat,                 &  !Number of atoms ([df])
                                    nsp,                 &  !Number of species [df]
                                    nband,               &  !Number of bands indivdual spin [df]
                                    max_nbands,           &  !Maximum number of bands (For nspin = 2)
                                    gtot,                &  !Total of g-vectors [df]
                                    nr1, nr2, nr3,       &  !Dense Real FFT grid [df]
                                    nr1s, nr2s, nr3s,    &  !Smooth Real FFT grid [df]
                                    ngw,                 &  !Number of plane waves *and* G+k Gk-vectors 
                                    spin=0,              &  !spin (0 is not defined)
                                    nupdwn(2)               !total states up (1) and down(2)

   integer, allocatable          :: bcalc(:),            &  !Array of all bands to calculate (Command line)
                                    mill(:,:),           &  !Miller indices mill for G+k *only* (3, ngw) [gf]
                                    nls(:),              &  !Indices of the planes, above nls for G+k (ngw)
                                    nlsm(:)                 !Indices of planes, below gamma_only for G+k (ngw)
   ! 
   real(DP)                      :: alat,                &  !Lattice constant
                                    ht(3,3),             &  !real cell matrix [df]
                                    at(3,3),             &  !real scaled cell matrix
                                    bg(3,3)                 !reciprocal scaled matrix

   real(DP), allocatable         :: tau(:,:),            &  !real atomic positions tau(3, nat) [df]
                                    g(:,:),              &  !G-Vectors for G+k *only* g(3,ngw) [gf]
                                    lambda(:,:)             !Lambda Matrix (only for CP calculation)
   !
   character(len=3), allocatable :: typ(:)                  !Atomic spcies types typ(nat) [df]
   character(len=256)            :: calculation,         &  !Calculation name 
                                    units,               &  !Atomic position units
                                    outtype,             &  !Output file type
                                    outroot,             &  !Output file root name [configfile]
                                    savedir,             &  !saved data directory in outdir (normally *.save)            
                                    datafile,            &  !Datafile name -> df 
                                    gvectorsfile,        &  !G-Vectors filename -> df 
                                    evcroot,             &  !evc root filename (command line)
                                    evcfile,             &  !evc filename -> ef 
                                    evcext,              &  !The file extension for the evcfile
                                    lambdafile,          &  !Lambda Matrix file (not needed for PW)
                                    lambdaext,           &  !Lambda Matrix extension
                                    kptdir                  !KPoint Dir name (K0001)
   !
   logical                       :: lpwscf,              &  !Flag for PWSCF calculations
                                    lcp,                 &  !Flag for CP calcualtions
                                    gamma_only,          &  !Gamma only
                                    lrot=.true.,         &  !Rotation using Lambda matrix required
                                    debug                   !print out array values to files
   ! 
   !Configuration file configfile
   namelist /config/ outroot, outtype, evcfile, evcext, lambdaext, debug
   !
   call hello()
   call user_input()
   call parse_qe_calc_data()
   call gvec_setup()
   call evc_main_loop()
   !
   contains
      !
      !
      !>===============================================================================<!
      subroutine user_input()
      !>===============================================================================<!
         !
         ! 1) Read in all command line values
         ! 2) Read configfile
         !
         integer                       :: i, j,    &  
                                          tot,     &  !Total command line arguments
                                          itot,    &  !Total arguments counter
                                          ipos,    &  !Positional Arguments counter
                                          ierr
         !
         character(len=256)            :: buffer      !Temp buffer array
         character(len=100)            :: arg,     &  !argument string
                                          targ        !temp argument string (used of counting bands)
         !
         !-------------------------------------------
         !Check command line numbers
         !-------------------------------------------
         tot = command_argument_count()
         if (tot < NUM_CLI) then
            write(*,*) 
            write(*,'(/, X, A, I3)') trim(print_red('ERROR')) // &
               & ': Incorrect number of Command line arugments. Expected at least 3, got', tot
            call print_usage()
            write(*,*) 
            stop
         endif
         !-------------------------------------------
         !
         !-------------------------------------------
         !Read and echo each of the command line values
         !-------------------------------------------
         write(*,*) 
         write(*,*) '=Command Line values='
         itot = 1
         ipos = 1
         bnum = 0 
         do
            CALL get_command_argument(itot, arg)
            if (len_trim(arg) == 0) exit
            !
            !Flagged Command line arguments
            if (index(arg, '--') /= 0) then 
               !
               !spin
               select case (arg)
                  case('--spin')
                     itot = itot +1
                     CALL get_command_argument(itot, arg, status=ierr)
                     write(*,'(" spin:                        ", 30A)') trim(arg)
                     read(arg, *) spin
                     if (spin /= 1 .and. spin /= 2) then
                        write(*, '(/, A)') trim(print_red('ERROR')) // ': spin can only be 1 or 2'
                        stop
                     endif
                  case('--norot')
                     lrot = .false.
                     write(*,'(" Force No Rotation:           ", 30A)') trim('TRUE')
                  case default 
                     write(*,*) trim(print_red('ERROR')) //': Unkown flag: ', trim(arg)
                     call print_usage()
                     stop
               end select
            !
            !Positional Command line arguments
            else
               select case (ipos)
                  !               
                  !savedir
                  case (1)
                     savedir = trim(arg)
                     write(*,'(" QE savedir:                  ", 30A)') trim(arg)
                  !
                  !K-point
                  case (2)
                     read(arg, *) kpt
                     if (kpt >0) then
                        write(*,'(" K-point:                     ", 30A)') trim(arg)
                     else
                        write(*,*) trim(print_red('ERROR')) //': K-Point must be greater then 0!'
                     endif
                  !
                  !evcfile name
                  case (3)
                     evcroot = trim(arg)
                     if ( evcroot /= 'evc' .and. evcroot /= 'evc0' .and. evcroot /= 'evcm') then
                        write(*, '(/, A)') trim(print_red("ERROR")) //": Currently only evc fils allowed : 'evc', 'evc0', 'evcm'"
                        stop
                     endif
                     write(*,'(" evcfile:                     ", 30A)') trim(arg)
                  !
                  !Number of Bands to plot (optional)
                  case (4)
                     !Get the total number of positional command line arguments
                     j=1
                     do
                        CALL get_command_argument(itot+j, targ, status=ierr)
                        if (len_trim(targ) == 0) exit
                        j = j + 1
                     enddo
                     bnum = j
                     allocate(bcalc(bnum))

                     !Plot particular bands -> read the rest of the command line
                     write(*,'(" Bands indices:               ")', advance='no')
                     !First of the bands (already read into arg)
                     read(arg,*) bcalc(1)
                     write(*,'(A)', advance='no') trim(arg)//' '
                     !Read in the rest
                     do j=1,bnum-1
                        CALL get_command_argument(itot+j, arg, status=ierr)
                        read(arg,*) bcalc(j+1)
                        write(*,'(A)', advance='no') trim(arg)//' '
                     enddo
                     write(*,*)
                     !
               end select
               ipos = ipos + 1
            endif
            itot = itot + 1
         enddo
         !
         !If the bands were absent alert user
         if (bnum == 0) then
            write(*,'(" Bands to plot:               ", 30A)') trim('All')
         endif
         write(*,*)
         !-------------------------------------------
         !
         !-------------------------------------------
         !Default configfile values
         !-------------------------------------------
         evcext = 'dat'
         lambdaext = 'dat'
         outtype = 'xsf'
         !output file labels
         if (spin == 1) then
            outroot = 'KS_UP_' 
         elseif (spin == 2) then
            outroot = 'KS_DW_'
         else
            outroot = 'KS_'
         endif
         debug = .false.
         !-------------------------------------------
         !
         !-------------------------------------------
         !Open/Read configfile
         !-------------------------------------------
         write(*,*)
         write(*,*) '=Configuration values='
         open(unit=1, file=(trim(configfile)), action='read', status='old', iostat=ierr)
         if (ierr == 0) then
            write(*,'(" ", A, " found")') trim(configfile)
            read(1, nml=config)
         else
            write(*,'( " ", A, " not found, using default values")') trim(configfile)
         endif

         write(*,'(" Output type:                 ", 30A)') trim(outtype)
         write(*,'(" Output Root:                 ", 30A)') trim(outroot)
         write(*,'(" evcfile Extension:           ", 30A)') trim(evcext)
         write(*,'(" Lambda Extension:            ", 30A)') trim(lambdaext)
         !-------------------------------------------
         !
         !-------------------------------------------
         !Set-up some files
         !-------------------------------------------
         write(kptdir, '(I0.5)') kpt
         kptdir = 'K' // kptdir
         datafile = trim(savedir) // '/' // trim(dataroot)
         gvectorsfile = trim(savedir) // '/' // trim(kptdir) // '/' // trim(gkroot)
         if (spin == 0) then
            evcfile = trim(savedir) // '/' // trim(kptdir) // '/' // trim(evcroot) // '.' // trim(evcext)
         else
            write(buffer, '(I1)') spin
            evcfile = trim(savedir) // '/' // trim(kptdir) // '/' // trim(evcroot) // trim(buffer) // '.' // trim(evcext)
         endif
         !-------------------------------------------
         !
      end subroutine user_input
      !>===============================================================================<!
      !
      !
      !>===============================================================================<!
      subroutine parse_qe_calc_data()
      !>===============================================================================<!
         !
         ! 1) Read QE calculation data files (Not evc, yet)
         ! 2) echo read-in variables
         integer                       :: i, j, k, temp,    &
                                          read_spin,        &  !temp spin variable
                                          ierr
         !
         character(iotk_attlenx)       :: attr                 !iotk attribute value
         character(len=256)            :: buffer,           &  !Temp buffer arrray
                                          steptag,          &  !step label for lambda file
                                          spintag              !spin label for lambda file
         !
         logical                       :: found
         !
         write(*,*)
         write(*,*) '=Reading files='
         !
         !---------READING FILE: DATA-FILE-----------
         !-------------------------------------------
         write(*,'(" Reading Data File:           ", 30A)') trim(datafile)
         call iotk_open_read(dunit, file=trim(datafile), ierr=ierr)
            if (ierr > 0) then
               call error(trim(datafile)// ' Not found!')
            endif
            !
            !
            !--HEADER-------------------------------
            call iotk_scan_begin(dunit, 'HEADER', found=found)
               if (.not. found) then
                  call error('HEADER tag Not found in ' // trim(datafile) // '!')
               endif
               call iotk_scan_empty(dunit, 'CREATOR', ATTR=attr)
                  call iotk_scan_attr( attr, 'NAME', calculation)
                  if (calculation == 'PWSCF') then
                     lpwscf = .true.
                     lrot = .false.
                  elseif (calculation == 'CP') then
                     lcp = .true.
                  else 
                     write(*,*) trim(print_red('ERROR')) //': Current calculation ', trim(calculation), ' is unspported'
                     stop
                  endif

            call iotk_scan_end(dunit, 'HEADER')
            !
            !Warn if using wrong evcfile for the calcualtion type
            if (lcp .and. (evcroot /= 'evc0' .and. evcroot /= 'evcm')) then
               write(*,*) trim(print_red('Warning')) // ': previous calculation was CP but using a ' & 
                  & // "non-CP evcfile, ",  trim(evcroot)
               lrot = .false.
            elseif (lpwscf .and. evcroot /= 'evc') then
               write(*,*) trim(print_red('Warning')) // ': previous calculation was PWSCF but using a ' & 
                  & // "non-PWSCF evcfile, ",  trim(evcroot)
            endif
            !
            !--CELL---------------------------------
            call iotk_scan_begin(dunit, 'CELL', found=found)
               !
               if (.not. found) then
                  call error('CELL tag Not found in ' // trim(datafile) // '!')
               endif
               !
               call iotk_scan_dat(dunit, 'LATTICE_PARAMETER', alat)
               call iotk_scan_begin(dunit, 'DIRECT_LATTICE_VECTORS')
                  call iotk_scan_dat(dunit, "a1", ht(:,1))
                  call iotk_scan_dat(dunit, "a2", ht(:,2))
                  call iotk_scan_dat(dunit, "a3", ht(:,3))
               call iotk_scan_end(dunit, 'DIRECT_LATTICE_VECTORS')
               call iotk_scan_begin(dunit, 'RECIPROCAL_LATTICE_VECTORS')
                  call iotk_scan_dat(dunit, "b1", bg(:,1))
                  call iotk_scan_dat(dunit, "b2", bg(:,2))
                  call iotk_scan_dat(dunit, "b3", bg(:,3))
               call iotk_scan_end(dunit, 'RECIPROCAL_LATTICE_VECTORS')
            call iotk_scan_end(dunit, 'CELL')
            !
            !
            !--IONS---------------------------------
            call iotk_scan_begin(dunit, 'IONS', found=found)
               !
               if (.not. found) then
                  call error('IONS tag Not found in ' // trim(datafile) // '!')
               endif
               !
               call iotk_scan_dat (dunit, 'NUMBER_OF_ATOMS', nat)
               call iotk_scan_dat (dunit, 'NUMBER_OF_SPECIES', nsp)
               call iotk_scan_empty(dunit, 'UNITS_FOR_ATOMIC_POSITIONS', attr )
               call iotk_scan_attr(attr, "UNITS", units )
               !
               allocate(typ(nat))
               allocate(tau(3,nat))
               !
               do i=1,nat
                  call iotk_scan_empty(dunit, "ATOM" // TRIM(iotk_index( i )), attr )
                  call iotk_scan_attr(attr, "SPECIES", typ(i) )
                  call iotk_scan_attr(attr, "tau", buffer )
                  read(buffer, *) tau(1:3, i)
                  !Make sure the initial coordinates are in Bohr
                  if (units == 'Angstrom') then
                     tau(1:3, i) =  tau(1:3, i) / BOHR_TO_ANGSTROM
                  endif
               enddo
               !
            call iotk_scan_end(dunit, 'IONS')
            !
            !Scaled lattice vectors
            at = ht/alat
            !
            !
            !--PLANE_WAVES--------------------------
            call iotk_scan_begin(dunit, 'PLANE_WAVES', found=found)
               !
               if (.not. found) then
                  call error('PLANE_WAVES tag Not found in ' // trim(datafile) // '!')
               endif
               !
               call iotk_scan_dat(dunit, "SMOOTH_GVECT_NUMBER", gtot)
               call iotk_scan_empty(dunit, 'SMOOTH_FFT_GRID', ATTR=attr)
                  call iotk_scan_attr( attr, 'nr1s', nr1)
                  call iotk_scan_attr( attr, 'nr2s', nr2)
                  call iotk_scan_attr( attr, 'nr3s', nr3)
               call iotk_scan_dat(dunit, "GAMMA_ONLY", gamma_only)
               call iotk_scan_dat(dunit, "MAX_NUMBER_OF_GK-VECTORS", ngw)
            call iotk_scan_end(dunit, 'PLANE_WAVES')
            !
            !Important: we are only concerned with this particular G+k grid => We do NOT
            !Need the entire mill, g, nls, or nlsm!!
            allocate(mill(3, ngw)) 
            allocate(g(3, ngw)) 
            allocate(nls(ngw))
            allocate(nlsm(ngw))
            !
            !
            !--BAND_STRUCTURE_INFO------------------
            call iotk_scan_begin(dunit, 'BAND_STRUCTURE_INFO', found=found)
               !
               if (.not. found) then
                  call error('BAND_STRUCTURE_INFO tag Not found in ' // trim(datafile) // '!')
               endif
               !
               call iotk_scan_dat(dunit, 'NUMBER_OF_SPIN_COMPONENTS', read_spin)
               !
               !Check spins
               if (spin > 0 .and. read_spin /= 2) then
                  write(*,'(/, A, ": Number of spin components from ", A  " (", I1, ") do not match spin (", I1, ")")') &
                     trim(print_red("ERROR")), trim(datafile), read_spin, spin
                  stop
               endif
               !
               !Beware: This reads the max number of bands in a nspin = 2 calculation
               call iotk_scan_dat(dunit, 'NUMBER_OF_BANDS', max_nbands, attr=attr)
               !Read the spin states
               if (spin /= 0) then
                  call iotk_scan_attr( attr, 'UP', nupdwn(1))
                  call iotk_scan_attr( attr, 'DW', nupdwn(2))
               endif
            call iotk_scan_end(dunit, 'BAND_STRUCTURE_INFO')
            !
            !Number of executable bands
            if (spin > 0) then
               nband = nupdwn(spin)
            else
               nband = max_nbands
            endif
            !
            !If bnum is zero, calculate all bands (VERY memory consuming)
            if (bnum  == 0) then
               allocate(bcalc(nband))
               do i=1, nband
                  bcalc(i) = i
               enddo
            endif
            !
            !Check to make sure it's not above nband
            do i=1, size(bcalc)
               if (bcalc(i) > nband) then
                  write(*,'("Error: input bands ", I5, " above actual bands ", I5)') bcalc(i), nband 
                  stop
               endif
            enddo
            !
            !----------READING FILE: LAMBDA-------------
            !                CP ONLY!!
            !-------------------------------------------
            if (lcp) then
               if (lrot) then
                  !Note: QE allocates the lambda matrix according to the largest band number
                  allocate(lambda(max_nbands, max_nbands))
                  !
                  !0 or m label of the lmabda matrix
                  if (evcroot .eq. 'evc0') then
                     write(steptag, '(A)') '0'
                  elseif (evcroot .eq. 'evcm') then
                     write(steptag, '(A)') 'm'
                  else
                     write(steptag, '(A)') ''
                  endif
                  !
                  !The 0 or 1 (always at least 0) of the lambda matrix
                  if (spin == 0 .or. spin == 1) then
                     temp = 1
                     write(spintag, '(I1)') 1
                  else
                     temp = 2
                     write(spintag, '(I1)') spin
                  endif
                  !
                  !Lambda file, but the name is not really needed
                  lambdafile = trim(savedir) // '/' // trim(kptdir) // '/'  // trim(lambdaroot) &
                     &// trim(steptag) // trim(spintag) //'.' // trim(lambdaext)
                  !
                  write(*,'(" Reading Lambda File:         ", 30A)') trim(lambdafile)
                  call iotk_scan_begin(dunit, 'EIGENVECTORS', found=found)
                     !
                     if (.not. found) then
                        call error('EIGENVECTORS tag Not found in ' // trim(datafile) // '!')
                     endif
                     !
                     call iotk_scan_begin(dunit, 'K-POINT'// trim(iotk_index(kpt)))
                        call iotk_scan_dat (dunit, "LAMBDA" // trim(steptag) // TRIM(iotk_index(temp)), lambda, found=found)
                        !
                        if (.not. found) then
                           call error('LAMBDA file, '// trim(lambdafile) // ', not found!')
                        endif
                        !
                     call iotk_scan_end(dunit, 'K-POINT'// trim(iotk_index(kpt)))
                  call iotk_scan_end(dunit, 'EIGENVECTORS')
                  !
               else
                  write(*,'(" Skipping Lambda File...    ")')
               endif
            endif
            !-------------------------------------------
            !
         call iotk_close_read(dunit)
         ! 
         !-------------------------------------------
         !
         !
         !----------READING FILE: G-VECTORS----------
         !-------------------------------------------
         write(*,'(" Reading GK-Vector File:      ", 30A)') trim(gvectorsfile)
         call iotk_open_read(gunit, file=trim(gvectorsfile), ierr=ierr)
            !
            if (ierr > 0) then
               call error('GK-VECTORS file, ' //trim(gvectorsfile)// ' Not found!')
            endif
            !
            call iotk_scan_dat (gunit, "GRID", mill)
         call iotk_close_read(gunit)
         !-------------------------------------------
         !
         !
         write(*,*) ''
         !
         !-------------------------------------------
         !Echo all variables
         !-------------------------------------------
         write(*, '(" =QE Variables=")')
         write(*,'(" Calculation Type:             ", A8)') adjustr(trim(calculation))
         write(*,'(" Number of Atoms:              ", I8)') nat
         write(*,'(" Number of Species:            ", I8)') nsp
         write(*,'(" Lattice Constant:             ", F8.3)') alat
         if (spin  == 1) then
            write(*,'(" Number Spin UP Bands:         ", I8)') nband
         elseif (spin  == 2) then
            write(*,'(" Number Spin DOWN Bands:       ", I8)') nband
         else
            write(*,'(" Number of TOTAL Bands:        ", I8)') nband
         endif
         write(*,'(" Planewaves (G+k Points):      ", I8)') ngw
         write(*,'(" Total Gird Points:            ", I8)') gtot
         write(*,'(" Gamma Only:                   ", L8)') gamma_only
         write(*,*)
         !-------------------------------------------
         !
      end subroutine parse_qe_calc_data
      !>===============================================================================<!
      !
      !
      !
      !
      !>===============================================================================<!
         subroutine gvec_setup()
      !>===============================================================================<!
         !
         !Calculate the wavfunctions from c, nls (nlsm), mill and g -> psi
         !Take the inverse Fourier transform
         !
         integer :: i, j, k, ng
         integer :: n1, n2, n3, n1s, n2s, n3s
         !
         !Calculate the g-vectors:
         do ng = 1, ngw
            i = mill(1, ng)
            j = mill(2, ng)
            k = mill(3, ng)
            g (1:3,ng) = i*bg(:,1) + j*bg(:, 2) + k*bg(:, 3)
         enddo
         !
         !Calculate nls
         DO ng = 1, ngw
            n1 = nint (sum(g(:, ng) * at(:, 1))) + 1
            mill (1,ng) = n1 - 1
            n1s = n1
            IF (n1<1) n1 = n1 + nr1
            IF (n1s<1) n1s = n1s + nr1
            !
            n2 = nint (sum(g(:, ng) * at(:, 2))) + 1
            mill (2,ng) = n2 - 1
            n2s = n2
            IF (n2<1) n2 = n2 + nr2
            IF (n2s<1) n2s = n2s + nr2
            !
            n3 = nint (sum(g(:, ng) * at(:, 3))) + 1
            mill (3,ng) = n3 - 1
            n3s = n3
            IF (n3<1) n3 = n3 + nr3
            IF (n3s<1) n3s = n3s + nr3
            !
            IF (n1>nr1 .or. n2>nr2 .or. n3>nr3) &
               write(*,*) 'ggen Mesh too small?', n1, nr1, n2, nr2, n3, nr3
            !
            IF (ng <= ngw) &
               nls (ng) = n1s + (n2s - 1)*nr1 + (n3s - 1)*nr1*nr2
         ENDDO
         !
         !Calculate nlsm
         if (gamma_only) then
            DO ng = 1, ngw
               n1 = -mill (1,ng) + 1
               n1s = n1
               IF (n1 < 1) THEN
                  n1 = n1 + nr1
                  n1s = n1s + nr1
               END IF
               !
               n2 = -mill (2,ng) + 1
               n2s = n2
               IF (n2 < 1) THEN
                  n2 = n2 + nr2
                  n2s = n2s + nr2
               END IF
               !
               n3 = -mill (3,ng) + 1
               n3s = n3
               IF (n3 < 1) THEN
                  n3 = n3 + nr3
                  n3s = n3s + nr3
               END IF
               !
               IF (n1>nr1 .or. n2>nr2 .or. n3>nr3) THEN
                  write(*,*) 'index_minusg Mesh too small?'
                  stop
               ENDIF
               !
               IF (ng<=ngw) &
                  nlsm(ng) = n1s + (n2s - 1) * nr1 + (n3s-1) * nr1 * nr2
            ENDDO
         ENDIF
         !
      end subroutine gvec_setup
      !>===============================================================================<!
      !
      !
      !
      !
      !TODO Add a Parallel Loop
      !>===============================================================================<!
         Subroutine evc_main_loop()
      !>===============================================================================<!
         !
         ! Main evc loop
         ! 1) Read the file
         ! 2) Calculate the wavefunction
         ! 3) Print the file
         !
         use rotation,        only : rotate
         use fft,             only : cfft3d
         !
         integer                       :: i, j, ierr
         !
         complex(DP), allocatable      :: c(:,:),           &  !Planewave coefficients c(ngw, nband) [ef]
                                          crot(:,:),        &  !Rotated planewave coefficients crot(ngw, nband)
                                          psi(:)               !wavefunction  psi(nr1*nr2*nr3)
         !
         !
         real(DP)                      :: scalef               !Scale factor for the evc
         !
         character(iotk_attlenx)       :: attr                 !iotk attribute value
         character(len=5)              :: label1, label2
         !
         logical                       :: found
         !
         allocate(c(ngw, nband))
         allocate(psi(nr1*nr2*nr3))
         !
         write(*,*) "=Wavefunction Loop="
         write(*,'(" Reading EVC File:            ", 30A)') trim(evcfile)
         write(*,'(" Estimated size of c(ngw, nbands): ",f10.2, " MiB ")') &
            complex_size*nband*ngw/dble(Mb)

         !Read in all Planewave coefficients for G+K
         c = 0.0
         call iotk_open_read(eunit, file=trim(evcfile), ierr=ierr)
            !
            if (ierr > 0) then
               call error('EVC file, ' //trim(evcfile)// ' Not found!')
            endif
            !
            call iotk_scan_empty(eunit, 'INFO', ATTR=attr)
            call iotk_scan_attr( attr, 'scale_factor', scalef)
            do i=1,nband
               call iotk_scan_dat(eunit, "evc" // TRIM(iotk_index(i)), c(:, i))
            enddo
         call iotk_close_read(eunit)
         !
         if (lrot) then
            write(*,'(/, " Performing Lambda Rotation...")')
            write(*,'(" Estimated size of lambda(nbands, nbands): ",f10.2, " MiB ")') &
            complex_size*nband*nband/dble(Mb)

            allocate(crot(ngw, nband))
            call rotate(crot, c, nband, lambda)
            c = crot
            deallocate(crot)
         endif
         !
         write(*,'(/, " Calculating Wavefunctions ...", /)')
         !Calculate each of the wavefunctions from the read-in 
         !G+k Planewave coefficients.
         do i=1,size(bcalc)
               write(label1, '(I5)') i
               write(label2, '(I5)') bcalc(i)
               !
               write(*,'(1X, A, ") State: ", A)') trim(adjustl(label1)), trim(adjustl(label2))
               !
               !Convert c to psi
               write(*,'(" Calculating c -> psi...")')
               psi = 0.0
               call c2psi(c(:, bcalc(i)), psi)
               !
               !Calculate InvFFT
               write(*,'(" Calculating InvFFT...")')
               call cfft3d(psi(:), nr1, nr2, nr3, nr1, nr2, nr3, 1)
               !
               write(*,'(" Integrated Charge:", F7.4)') sum(dble(psi)**2)/(nr1*nr2*nr3)
               !
               !Write file
               if (outtype == 'xsf') then
                  write(*,'( " Writing XSF File: ")', advance='no')
                  call write_xsf(bcalc(i), psi)
               endif
               !
               write(*,*)
         enddo
         !
      end subroutine evc_main_loop
      !>===============================================================================<!
      !
      !
      !
      !
      !>===============================================================================<!
         subroutine c2psi(c, psi)
      !>===============================================================================<!
         !
         !Convert c -> psi
         !
         complex(DP), intent(in)    :: c(:)
         complex(DP), intent(out)      :: psi(:)
         !
         integer  :: i, j
         !
         !TODO Add no gamma
         if (gamma_only) then
            do j = 1, ngw
               psi(nlsm(j)) = CONJG(c(j))
               psi(nls(j)) = c(j)
            end do
         endif
         !
      end subroutine c2psi
      !>===============================================================================<!
      !
      !
      !
      !
      !>===============================================================================<!
         subroutine write_xsf(nb, psi)
      !>===============================================================================<!
         !
         integer, intent(inout)        :: nb       !Current band index *within* bcalc
         complex(DP), intent(in)       :: psi(:)   !Current psi (isosurface)
         !
         integer                :: i, j, k, xunit, ierr
         !
         character(len=256)     :: outfile
         character(len=3)       :: x1
         !
         !
         !Output file info
         xunit = 100 + nb
         write(x1, '(I0)'), nb
         outfile = TRIM(outroot)//TRIM(x1)//'.'//outtype
         open(unit=xunit, file=TRIM(outfile),iostat=ierr)

         write(*,'(A)') trim(outfile)
         !
         !Print the Atomic positions
         write(xunit,'(A)') 'CRYSTAL'
         write(xunit,'(A)') 'PRIMVEC'
            do i=1,3
               write(xunit,'(3(F12.5))') (ht(j,i)*BOHR_TO_ANGSTROM, j=1,3)
            enddo
         write(xunit,'(A)') 'PRIMCOORD'
         write(xunit,*) nat, '1'
         do i=1,nat
            write(xunit,'(A, 3(F12.5))') trim(typ(i)), (tau(j,i)*BOHR_TO_ANGSTROM, j=1,3)
         enddo
         !
         !Print the electronic density
         write(xunit,*) 'BEGIN_BLOCK_DATAGRID_3D'
         write(xunit, *) '3D_PWSCF'
         write(xunit,*) 'DATAGRID_3D_UNKNOWN'
         write(xunit,*) nr1, nr2, nr3
         write(xunit, *) '0.000  0.000 0.000'
         do i=1,3
            write(xunit,'(3(F12.5))') (ht(j,i)*BOHR_TO_ANGSTROM, j=1,3)
         enddo
         do i=1,nr1*nr2*nr3
            write(xunit,*), dble(psi(i))**2
         enddo
         write(xunit,*) 'END_DATAGRID_3D'
         write(xunit,*) 'END_BLOCK_DATAGRID_3D'
         close(xunit)
         !
      end subroutine write_xsf
      !>===============================================================================<!
      !
      !
      !
      !
      !>===============================================================================<!
      subroutine error(text)
         !
         implicit none
         !
         character(len=*), intent(in)        :: text
         !
         write(*,*)
         write (*,*) trim(print_red('Error')) // ': ' // trim(text)
         stop
         !
      end subroutine
      !>===============================================================================<!
      !
      !
      !
      !
      !>===============================================================================<!
      !
      !
      !
      !
      !
      !>===============================================================================<!
      subroutine print_usage()
      !>===============================================================================<!
         !
         !Print program usage 
         !

         write(*,*) " "
         write(*,*) "Usage:"
         write(*,*) " "
         write(*,*) 'isoprint.x  savedir  kpoint  evcfile  [--norot] [--spin NUM] [band1 band2 ... bandN]'
         write(*,*) " "
         write(*,*) '  QE-savedir   previous QUANTUM-ESPRESSO save directory (usually *.save)'
         write(*,*) ''  
         write(*,*) '  kpt          K-Point number, used to determine the K<NUM> sub-directory of QE-savedir.'
         write(*,*) '               This directory will be used to (1) read the gkvectors.dat file which contains '
         write(*,*) '               details on the particular G+k grid we are interested in (Note: we do NOT need' 
         write(*,*) '               OR Calculate information for the entire G grid,  only for G+k grid), and (2)' 
         write(*,*) '               the evc file which contains the planewave coefficients (see next).'
         write(*,*) ' ' 
         write(*,*) "  evcfile      Planewave coefficients filetype (Not Path) could be 'evc', 'evc0', or 'evcm'"
         write(*,*) "  "
         write(*,*) "  bands        (Optional) Natural index (starting from 1) of bands to be plotted. If not"
         write(*,*) "               included *ALL* Bands will be plotted."
         write(*,*) " " 
         write(*,*) "  --spin NUM   (Optional) For multi-spin calculations (nspin = 2) specify 1 ('UP') or 2 ('DW')."
         write(*,*) "               This will cause the program to look for the corresponding spin-component varaibles"
         write(*,*) " "
         write(*,*) "  --norot      (Optional) Do not perform a lambda matrix rotation (CP-type calculations only)"
         write(*,*) " "
         write(*,*) " "
         write(*,*) "See the documentation for more information"
               !
      end subroutine
      !>===============================================================================<!
      !
      !
      !
      !
      !>===============================================================================<!
      character(len=256) function print_red(text)
      !>===============================================================================<!
         !
         implicit none
         !
         character(len=*), intent(in)        :: text
         !
         integer                             :: temp, ierr
         !
         !call EXECUTE_COMMAND_LINE("tput colors &>/dev/null", exitstat=ierr)
         call system("tput colors 1>/dev/null 2>/dev/null", status=ierr)
         if (ierr == 0) then
            print_red = trim(achar(27)//'[1;31m'//text//achar(27)//'[0m')
         else
            print_red = trim('!!!! '//text//' !!!!')
         endif
         !
      end function print_red
      !>===============================================================================<!
      !
      !
      !
      !
      !>===============================================================================<!
         subroutine hello()
      !>===============================================================================<!

         write(*,*)
         write(*,'("==========================================")') 
         write(*,'("    ____           ____       _       __  ")')
         write(*,'("   /  _/________  / __ \_____(_)___  / /_ ")')
         write(*,'("   / // ___/ __ \/ /_/ / ___/ / __ \/ __/ ")')
         write(*,'(" _/ /(__  ) /_/ / ____/ /  / / / / / /_   ")')
         write(*,'("/___/____/\____/_/   /_/  /_/_/ /_/\__/   ")')
         write(*,'("==========================================")')
         write(*,*)
         write(*,'(" Charles W. Swartz VI")')
         write(*,'(" June 2014")')
         write(*,*)
      end subroutine hello
      !>===============================================================================<!
      !
      !
      !
      !
end program main
