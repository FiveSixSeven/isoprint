include make.sys

default :
	@echo 'to install, type at the shell prompt:'
	@echo '  ./configure'
	@echo '  make target'
	@echo ' '
	@echo 'where target is one of the following:'
	@echo '  isoprint      Generate isosurface plots from QE restart files'
	@echo '  all           isoprint'
	@echo '  clean         '
	@echo '  veryclean     ' 

###########################################################
# Main targets
###########################################################
isoprint : bindir liblapack libblas libiotk 
	if test -d src ; then \
	( cd src ; if test "$(MAKE)" = "" ; then make $(MFLAGS) TLDEPS= all ; \
	else $(MAKE) $(MFLAGS) TLDEPS= all ; fi ) ; fi

all   : isoprint

###########################################################
# Auxiliary targets used by main targets:
# compile modules, libraries, directory for binaries, etc
###########################################################
bindir :
	test -d bin || mkdir bin

#############################################################
# Targets for external libraries
############################################################
libblas : touch-dummy
	cd install ; $(MAKE) $(MFLAGS) -f extlibs_makefile $@

liblapack: touch-dummy
	cd install ; $(MAKE) $(MFLAGS) -f extlibs_makefile $@

libiotk: touch-dummy
	cd install ; $(MAKE) $(MFLAGS) -f extlibs_makefile $@

touch-dummy :
	$(dummy-variable)

#########################################################
# "make links" produces links to all executables in bin/
# while "make inst" INSTALLDIR=/some/place" links all
# available executables to /some/place/ (must exist and
# be writable), prepending "qe_" to all executables (e.g.:
# /some/place/qe_pw.x). This allows installation of QE
# into system directories with no danger of name conflicts
#########################################################
inst : 
	( for exe in */*/*.x */bin/* ; do \
	   file=`basename $$exe`; if test "$(INSTALLDIR)" != ""; then \
		if test ! -L $(PWD)/$$exe; then ln -fs $(PWD)/$$exe $(INSTALLDIR)/qe_$$file ; fi ; \
		fi ; \
	done )

links : bindir
	( cd bin/ ; \
	rm -f *.x ; \
	for exe in ../*/*/*.x ../*/bin/* ; do \
	    if test ! -L $$exe ; then ln -fs $$exe . ; fi \
	done \
	)


#########################################################
# Other targets: clean up
#########################################################

# remove object files and executables
clean :
	touch make.sys 
	for dir in \
		src CPV Modules PP PW \
		ACFDT \
		clib flib pwtools upftools \
		dev-tools extlibs Environ \
	; do \
	    if test -d $$dir ; then \
		( cd $$dir ; \
		if test "$(MAKE)" = "" ; then make $(MFLAGS) TLDEPS= clean ; \
		else $(MAKE) $(MFLAGS) TLDEPS= clean ; fi ) \
	    fi \
	done
	- @(cd install ; $(MAKE) $(MFLAGS) -f plugins_makefile clean)
	- @(cd install ; $(MAKE) $(MFLAGS) -f extlibs_makefile clean)
	- /bin/rm -rf bin/*.x tmp
	- cd PW/tests; /bin/rm -rf CRASH *.out *.out? ; cd -
	- cd CPV/tests; /bin/rm -rf CRASH *.out *.out? 

# remove configuration files too
distclean veryclean : clean
	- @(cd install ; $(MAKE) $(MFLAGS) -f plugins_makefile veryclean)
	- @(cd install ; $(MAKE) $(MFLAGS) -f extlibs_makefile veryclean)
	- rm -rf install/patch-plumed
	- cd install ; rm -f config.log configure.msg config.status \
	CPV/version.h ChangeLog* intel.pcl */intel.pcl
	- cd install ; rm -fr autom4te.cache
	- cd pseudo; ./clean_ps ; cd -
	- cd install; ./clean.sh ; cd -
	- cd include; ./clean.sh ; cd -
	- rm -f espresso.tar.gz
	- for dir in Doc; do \
	    test -d $$dir && ( cd $$dir ; $(MAKE) $(MFLAGS) TLDEPS= clean ) \
	done
	- rm -rf make.sys
	- rm -rf ./bin

tar :
	@if test -f espresso.tar.gz ; then /bin/rm espresso.tar.gz ; fi
	# do not include unneeded stuff 
	find ./ -type f | grep -v -e /.svn/ -e'/\.' -e'\.o$$' \
             -e'\.mod$$' -e'\.a$$' -e'\.d$$' -e'\.i$$' -e'\.F90$$' -e'\.x$$' \
	     -e'~$$' -e'\./GUI' | xargs tar rvf espresso.tar
	gzip espresso.tar

depend:
	@echo 'Checking dependencies...'
	- ( if test -x install/makedeps.sh ; then install/makedeps.sh ; fi)

