#! /usr/bin/python
from __future__ import print_function, division
#=---------------------------------------------------------------------------=#
# IsoPrint shiftios.py:
# Shift previously generated xsf iso-surfaces 
#=---------------------------------------------------------------------------=#
__author__ = 'Charles W. Swartz VI'

import sys
import Visualize
import textwrap

def main(filename, aindex, outfile):
    """
        Center isosurface *.xsf files

        Take atomic configurations and isosurfaces from a previously created 
        *.xsf file and center (shift) them onto a given atom. The atom index 
        is defined in the original *.xsf file and runs as 1 ... N. Care should 
        be taken in selecting the atom to center upon. Centered values are
        either printed to a specified file named from (--outfile) or by appending
        '-mod' to the original file.
        
    """

    #aindex is the natural number index, tindex is the true index
    tindex = aindex - 1

    print(textwrap.dedent("""
      ==========================================
          ____           ____       _       __  
         /  _/________  / __ \_____(_)___  / /_ 
         / // ___/ __ \/ /_/ / ___/ / __ \/ __/ 
       _/ /(__  ) /_/ / ____/ /  / / / / / /_   
      /___/____/\____/_/   /_/  /_/_/ /_/\__/   
            
                  Post-Processing

      ==========================================

      Charles W. Swartz
      June 2014

      """))

    print("=Parameters=")
    print("Original File: {:>20}".format(filename))
    print("Atom Index:    {:>20d}".format(aindex))
    print("Output File:   {:>20}\n".format(outfile))

    plot = Visualize.XSF(filename, verbose=True)
    plot.build(outfile, cid=tindex)


def parse_argv(argv):
    import argparse
    import textwrap

    welcome = textwrap.dedent(main.__doc__)
    parser = argparse.ArgumentParser( 
        description=welcome, 
        formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument(
        'filename',
        action='store',
        type=str,
        help='Name of original *.xsf isosurface file'
    )
    parser.add_argument(
        'aindex',
        action='store',
        type=int,
        help='Index of atom to be centered (count starts at 1)'
    )
    parser.add_argument(
        '--outfile',
        action='store',
        type=str,
        default=None,
        help='Name of centered *.xsf output file [default: add "-mod" to original filename]'
    )

    print()
    args  = parser.parse_args()

    #Some data wrangling
    if not args.outfile:
        args.outfile = args.filename + '-mod'

    return args
    

if __name__ == '__main__':
    args = parse_argv(sys.argv[1:])
    main(args.filename, args.aindex, args.outfile)
